using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public Text scoreText;
    public Text bestText;

    public int points = 0;
    public int bestPoints = 0;

    public GameObject gameOverPanel;
    public GameObject readyPanel;


    // Use this for initialization
    void Start()
    {// playerprefs store value of variable

        if( ! PlayerPrefs.HasKey("bestScore"))
        {
             PlayerPrefs.SetInt("bestScore",0);
        }
       
        bestPoints = PlayerPrefs.GetInt("bestScore");
        bestText.text = "Best score: " + bestPoints.ToString();

       
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = points.ToString();
    }
    public void Replay()
    {
        gameOverPanel.SetActive(false);
        SceneManager.LoadScene("game");
    }

    
}
