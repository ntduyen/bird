using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour
{
    [SerializeField]
    private GameObject[] characters;

    [SerializeField]
    private GameObject [] guides;
    private int characterIndex;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeCharacter(int index)
    {
        for( int i = 0; i < characters.Length; i++)
        {
            characters[i].SetActive(false);
            guides[i].SetActive(false);
        }

        characters[index].SetActive(true);
        guides[index].SetActive(true);
        characterIndex = index;
    }

    public void StartGame()
    {
        PlayerPrefs.SetInt("characterIndex", characterIndex);
        SceneManager.LoadScene("Game");
    }
}
