using System;
using UnityEngine;

public class LoadCharacter : MonoBehaviour
{
    [SerializeField]
    private GameObject[] characterPrefabs;
    
     // Start is called before the first frame update
    void Start()
    {
        LoadBird();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void LoadBird()
    {
        int index = PlayerPrefs.GetInt("characterIndex");

        GameObject.Instantiate(characterPrefabs[index],transform.position, Quaternion.identity);
    }
}
