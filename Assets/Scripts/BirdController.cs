
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BirdController : MonoBehaviour
{
    Rigidbody2D rg;
    public float speed;

    public GameObject gameOverPanel;

    public GameObject bulletPrefab;
    public float shootInterval = 1f;
    private float shootTimer = 1f;
    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip flyClip, hitClip, pointClip;

    public GameController gc;

    private int characterIndex;


    //public float timeSlow;
    private bool isSlow;


    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        rg = GetComponent<Rigidbody2D>();
        gc = GameObject.FindGameObjectWithTag("gamecontroller").GetComponent<GameController>();
        gameOverPanel = GameObject.FindWithTag("gameover");
        characterIndex = PlayerPrefs.GetInt("characterIndex");

        gameOverPanel.SetActive(false);
        isSlow = false;
    }
    
    // Update is called once per frame
    void Update()
    {
        // fly
         if  (Input.GetKeyDown("space"))
//        if (Input.GetMouseButtonDown(0)|| Input.GetKeyDown("space"))
        {
            rg.AddForce(Vector2.up * speed );
            audioSource.PlayOneShot(flyClip);
        }
       
        // slow time
        if (Input.GetKeyDown("f") && characterIndex == 2 && !isSlow )
        {
            SlowDownTime(5f);
        }

        // shoot

        //if (Input.GetKeyDown(KeyCode.S) && characterIndex == 1)
        //{
        //    Instantiate(bulletPrefab, transform.position, transform.rotation);
        // }
        if (characterIndex == 1)
        {
            shootTimer += Time.deltaTime;
            if (shootTimer >= shootInterval)
            {
                 ShootBullet();
                 shootTimer = 0f;
            }
        }
    }
         
    private void ShootBullet()
    {
        // Create a new bullet GameObject at the bird's position
        GameObject newBullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);

        // Add velocity to the bullet to make it move
        Rigidbody2D bulletRb = newBullet.GetComponent<Rigidbody2D>();
        bulletRb.velocity = new Vector2(3f * 2f, 0f);
    }

    private void SlowDownTime(float duration)
    {
        Time.timeScale = 0.5f; // Set the time scale to 0.5 (slowing down the game to 1/2th of the original speed)
        speed = speed / Time.timeScale;
        StartCoroutine(ResetTimeScale(duration));
    }

    private System.Collections.IEnumerator ResetTimeScale(float duration)
    {
        yield return new WaitForSecondsRealtime(duration); // Wait for the specified duration
        Time.timeScale = 1; // Reset the time scale to its original value
        isSlow = true;
    }

    void GameOver()
    {
        Time.timeScale = 0;
        gameOverPanel.SetActive(true);
        if (PlayerPrefs.GetInt("bestScore") < gc.points)
            PlayerPrefs.SetInt("bestScore", gc.points);
    }

   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "pipe" || collision.gameObject.tag == "ground")
        {
            audioSource.PlayOneShot(hitClip);
            GameOver();
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("score"))
        {
            audioSource.PlayOneShot(pointClip);
            gc.points = gc.points + 1;
        }
    }
}

