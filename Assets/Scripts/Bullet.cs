
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 3f;
    public float lifespan = 2f;

    private void Start()
    {

    }

    private void Update()
    {
       // transform.Translate(Vector3.right * speed * Time.deltaTime); // Move the bullet forward
        lifespan -= Time.deltaTime;
        if (lifespan <= 0)
            Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="pipe") {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
            if (col.CompareTag("pipe"))
            {
                col.gameObject.SetActive(false);
                Destroy(gameObject);
            }
            

    }
    
}
