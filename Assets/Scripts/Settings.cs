using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Settings : MonoBehaviour
{
    public Slider musicSlider;
    public Slider SFXSlider;

    public AudioMixer myMixer;

    public GameObject panel;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("musicVolume"))
        {
            LoadVolume();
        }
        else
        {
            SetMusicVolume();
        }
        if (PlayerPrefs.HasKey("SFXVolume"))
        {
            LoadVolume();
        }
        else
        {
            SetSFXVolume();
        }
    }

    public void SetMusicVolume()
    {
       // Debug.Log(musicSlider.value);
        myMixer.SetFloat("music", Mathf.Log10(musicSlider.value) * 20);
        PlayerPrefs.SetFloat("musicVolume", musicSlider.value);
    }

    public void SetSFXVolume()
    {
        //Debug.Log(SFXSlider.value);
        myMixer.SetFloat("SFX", Mathf.Log10(SFXSlider.value) * 20);
        PlayerPrefs.SetFloat("SFXVolume", musicSlider.value);
    }
    void LoadVolume()
    {
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume");
        SFXSlider.value = PlayerPrefs.GetFloat("SFXVolume");

        SetMusicVolume();
        SetSFXVolume();
    }

    public void Pause()
    {
        Time.timeScale = 0;
        panel.SetActive(true);

    }
    public void Close()
    {
        panel.SetActive(false);
        Time.timeScale = 1;
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
